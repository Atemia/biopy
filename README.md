# biopy
Biological Python

Playing with micro biology in python using jupyter notebooks for class :)

##### install virtualenvwrapper
```
# apt install python-pip
# pip install --upgrade virtualenvwrapper
$ source /usr/local/bin/virtualenvwrapper.sh
$ mkdir ~/.virtualenvs && export WORKON_HOME=~/.virtualenvs
$ mkdir -p src/python && cd ~/src/python # create source directory
$ mkvirtualenv --python=python3 biopy
```

##### activate the biopy virtualenv
```
$ source ~/.virtualenvs/biopy/bin/activate # make sure the virtualenv is activated
```

##### install git
```
# apt install git
$ git clone https://gitlab.com/urbanslug/biopy.git
$ cd biopy
# apt install python3-dev
$ pip install --upgrade jupyter ipython
$ pip install --upgrade -r requirements/base.pip
```

##### Run the notebook
```
$ jupyter notebook
```

### Note:
 - always make sure the virtualenv is activated
