What is a function vs an expression/statement?

What is a loop:
 - loop vs recursion

control structures:
 - if then else
 - when
 - while 
 - for
 - switch statements
 
variables and constants:
 - define both
 
What's a REPL (Read Evaluate Print Loop) and how to use it?

Compiled vs interpreted languages

Python libraries:
 - what are they and how to use them?
 - pypi and pip
 - virtualenvs

Data types
 - lists, arrays, etc
   - list comprehensions
   - loops
      
File processing
 - take a fasta file and do shit

How to debug a program?

Python objects and classes
 -  what are objects and what are classes
 
Where to store code and how to:
 - version control
   * github and gitlab
   
Abstraction:
 - modules and files

Regular expressions intro:
